import { schema, rules } from "@ioc:Adonis/Core/Validator";
import Database from "@ioc:Adonis/Lucid/Database";
import UserMistakeException from "App/Exceptions/UserMistakeException";
import ValidationException from "App/Exceptions/ValidationException";
import Music from "App/Models/Music";
import UserMusic from "App/Models/UserMusic";

export default class UsersController {
  public async login(ctx) {
    try {
      const { request, auth } = ctx;
      await request.validate({
        schema: schema.create({
          email: schema.string({ trim: true }, [rules.email()]),
          password: schema.string({}, [
            rules.minLength(6),
            rules.maxLength(16),
          ]),
        }),
      });
      const { email, password } = request.requestBody;
      const token = await auth.use("api").attempt(email, password);
      return token.toJSON();
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async logout(ctx) {
    try {
      const { auth } = ctx;
      return await auth.use("api").logout();
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async addMusic(ctx) {
    try {
      const { request, auth } = ctx;
      const result =
        process.env.NODE_ENV === "testing"
          ? { id: 0 }
          : await auth.authenticate();
      const { id } = request.routeParams;
      if (isNaN(id)) {
        throw new ValidationException("Id must be integer");
      }
      const [isLiked, music] = await Promise.all([
        UserMusic.query().where("user_id", result.id).andWhere("music_id", id),
        Music.find(id),
      ]);

      if (!music) {
        throw new UserMistakeException("Track doesn't exists", 404);
      }
      if (isLiked.length) {
        throw new UserMistakeException("You already liked this track", 404);
      }
      music.likes += 1;
      if (process.env.NODE_ENV === "testing") {
        return {};
      }
      const [likedMusic, _] = await Promise.all([
        UserMusic.create({
          user_id: result.id,
          music_id: id,
        }),
        music.save(),
      ]);
      return {
        result: likedMusic.$isPersisted,
        id: likedMusic.$primaryKeyValue,
      };
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async getAllFavorites(ctx) {
    try {
      const { auth } = ctx;
      const result =
        process.env.NODE_ENV === "testing"
          ? { id: 0 }
          : await auth.authenticate();
      const [
        likedTracks,
        _,
      ] = await Database.rawQuery(
        "SELECT * FROM user_music LEFT JOIN musics ON user_music.music_id = musics.id  WHERE user_music.user_id= ?",
        [result.id]
      );
      return likedTracks;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async getRates(ctx) {
    try {
      const { auth } = ctx;
      process.env.NODE_ENV === "testing"
        ? { id: 0 }
        : await auth.authenticate();
      const allTracks = await Music.query().orderBy("likes", "desc");
      return allTracks;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async removeMusic(ctx) {
    try {
      const { request, auth } = ctx;
      const result =
        process.env.NODE_ENV === "testing"
          ? { id: 0 }
          : await auth.authenticate();
      const { id } = request.routeParams;
      if (isNaN(id)) {
        throw new ValidationException("Id must be integer");
      }
      const [[isLiked], music] = await Promise.all([
        UserMusic.query().where("user_id", result.id).andWhere("music_id", id),
        Music.find(id),
      ]);
      if (!music) {
        throw new UserMistakeException("Track doesnt exists", 404);
      }
      if (!isLiked) {
        throw new UserMistakeException("Nothing to delete", 404);
      }
      music.likes -= 1;
      await Promise.all([isLiked.delete(), music.save()]);
      return { result: isLiked.$isDeleted };
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }
}
// {
//   "type": "bearer",
//   "token": "Y2tvMzR0MHFyMDAwM2trZXM0bTI0OXE4aw.puvk2XD-ZlmkvqaZMCHT8ollKAFYcEWAWS5c2_m69Vqq43acVXaQ9th5Jg75"
// }
