import User from "../../Models/User";
import { schema, rules } from "@ioc:Adonis/Core/Validator";
import Music from "App/Models/Music";
import ValidationException from "App/Exceptions/ValidationException";
import UserMistakeException from "App/Exceptions/UserMistakeException";
import { HttpContext } from "@adonisjs/http-server/build/standalone";

export default class AdminsController {
  public async getAllUsers(ctx: HttpContext) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      const user = await User.all();
      return user;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async addUser(ctx) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      await request.validate({
        schema: schema.create({
          email: schema.string({ trim: true }, [
            rules.unique({
              table: "users",
              column: "email",
            }),
            rules.email(),
          ]),
          password: schema.string({}, [
            rules.minLength(6),
            rules.maxLength(16),
          ]),
        }),
      });
      const { email, password } = request.requestBody;
      const user = new User();
      user.email = email;
      user.password = password;
      await user.save();
      return { result: user.$isPersisted, id: user.$primaryKeyValue };
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async removeUser(ctx) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      const { id } = request.routeParams;
      if (isNaN(id)) {
        throw new ValidationException("Id must be integer");
      }
      const user = await User.find(id);
      if (!user) {
        throw new UserMistakeException("User not found", 400);
      }
      await user.delete();
      return user.$isDeleted;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async addMusic(ctx) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      await request.validate({
        schema: schema.create({
          name: schema.string({ trim: true }, [
            rules.minLength(6),
            rules.maxLength(16),
          ]),
          artist: schema.string({ trim: true }, [
            rules.minLength(6),
            rules.maxLength(16),
          ]),
        }),
      });
      const { name, artist } = request.requestBody;
      const music = new Music();
      music.name = name;
      music.artist = artist;
      await music.save();
      return { result: music.$isPersisted, id: music.$primaryKeyValue };
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async getAllMusic(ctx) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      const music = await Music.all();
      return music;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }

  public async removeMusic(ctx) {
    try {
      const { request } = ctx;
      if (
        process.env.NODE_ENV !== "testing" &&
        request.header("Authorization") !== process.env.MASTER_TOKEN
      ) {
        throw new UserMistakeException("Permission denied", 403);
      }
      const { id } = request.routeParams;
      if (isNaN(id)) {
        console.log("Ok");
        throw new ValidationException("Id must be integer");
      }
      const music = await Music.find(id);
      if (!music) {
        throw new UserMistakeException("Track doesn`t exists", 400);
      }
      await music.delete();
      return music.$isDeleted;
    } catch (error) {
      return error.handle
        ? error.handle(error, ctx)
        : ctx.response.status(500).send({ message: error.message });
    }
  }
}
