import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Music extends BaseModel {
  public static table = "musics";
  @column({ isPrimary: true })
  public id: number;

  @column()
  public name: string;

  @column()
  public artist: string;

  @column()
  public likes: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
