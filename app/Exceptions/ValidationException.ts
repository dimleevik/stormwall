import { HttpContext } from "@adonisjs/http-server/build/standalone";
import { Exception } from "@poppinss/utils";

/*
|--------------------------------------------------------------------------
| Exception
|--------------------------------------------------------------------------
|
| The Exception class imported from `@adonisjs/core` allows defining
| a status code and error code for every exception.
|
| @example
| new ValidationException('message', 500, 'E_RUNTIME_EXCEPTION')
|
*/
export default class ValidationException extends Exception {
  constructor(message: string) {
    super(message, 422);
    this.message = message;
  }
  public async handle(error: this, ctx: HttpContext) {
    ctx.response.status(error.status).send({ message: error.message });
  }
}
