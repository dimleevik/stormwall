import Route from "@ioc:Adonis/Core/Route";

export default Route.group(() => {
  Route.post("/login", "UsersController.login");
  Route.get("/logout", "UsersController.logout");
  Route.get("music/favorites", "UsersController.getAllFavorites");
  Route.get("music/rates", "UsersController.getRates");
  Route.post("music/:id", "UsersController.addMusic");
  Route.delete("music/:id", "UsersController.removeMusic");
}).prefix("user");
