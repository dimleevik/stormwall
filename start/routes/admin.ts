import Route from "@ioc:Adonis/Core/Route";

export default Route.group(() => {
  Route.get("user/", "AdminsController.getAllUsers");
  Route.post("user/", "AdminsController.addUser");
  Route.delete("user/:id", "AdminsController.removeUser");
  Route.post("music/", "AdminsController.addMusic");
  Route.get("music/", "AdminsController.getAllMusic");
  Route.delete("music/:id", "AdminsController.removeMusic");
}).prefix("admin");
