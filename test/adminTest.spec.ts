import test from "japa";
import supertest from "supertest";
const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`;

test.group("Admin routes test", () => {
  var id;
  test("Add music", async (assert) => {
    const { body } = await supertest(BASE_URL)
      .post("/admin/music")
      .send({ name: "testtest", artist: "testtesttest" })
      .expect(200);
    id = body.id;
    assert.equal(body.result, true);
  });

  test("Delete music", async () => {
    await supertest(BASE_URL).delete(`/admin/music/${id}`).expect(200);
  });

  test("Get users", async (assert) => {
    const { body } = await supertest(BASE_URL).get("/admin/user/").expect(200);
    assert.equal(Array.isArray(body), true);
  });

  test("Get music", async (assert) => {
    const { body } = await supertest(BASE_URL).get("/admin/music/").expect(200);
    assert.equal(Array.isArray(body), true);
  });

  test("Add user", async (assert) => {
    await supertest(BASE_URL)
      .post("/admin/user/")
      .send({ email: "testtest", password: "test" })
      .expect(422);
    const { body } = await supertest(BASE_URL)
      .post("/admin/user/")
      .send({ email: "test@mail.ru", password: "testtest" })
      .expect(200);
    id = body.id;
    assert.equal(body.result, true);
  });

  test("Delete user", async () => {
    await supertest(BASE_URL).delete(`/admin/user/${id}`).expect(200);
  });
});
