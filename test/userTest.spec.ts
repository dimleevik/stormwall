import test from "japa";
import supertest from "supertest";
const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`;

test.group("User routes test", () => {
  var id;
  test("Add music to favorite", async (assert) => {
    let { body } = await supertest(BASE_URL)
      .post("/admin/music")
      .send({ name: "testtest", artist: "testtesttest" })
      .expect(200);
    console.log("ok");
    id = body.id;
    let result = await supertest(BASE_URL)
      .post(`/user/music/${id}`)
      .expect(200);
    assert.equal(typeof result.body.length, "undefined");
  });

  test("Delete music from favorite", async (assert) => {
    const { body } = await supertest(BASE_URL)
      .delete(`/user/music/${id}`)
      .expect(404);
    assert.equal(body.message, "Nothing to delete");
    await supertest(BASE_URL).delete(`/admin/music/${id}`).expect(200);
  });

  test("Get rates", async (assert) => {
    const { body } = await supertest(BASE_URL)
      .get("/user/music/rates")
      .expect(200);
    assert.equal(Array.isArray(body), true);
  });

  test("Get favorites", async (assert) => {
    const { body } = await supertest(BASE_URL)
      .get("/user/music/favorites")
      .expect(200);
    assert.equal(body.length, 0);
  });
});
