import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Musics extends BaseSchema {
  protected tableName = "musics";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id").primary();
      table.string("name").notNullable();
      table.string("artist").notNullable();
      table.timestamps(false);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
