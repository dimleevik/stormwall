import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class UpdateMusics extends BaseSchema {
  protected tableName = "musics";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer("likes").defaultTo(0);
    });
  }

  public async down() {
    this.schema.alterTable(this.tableName, (table) => {
      table.dropColumn("likes");
    });
  }
}
