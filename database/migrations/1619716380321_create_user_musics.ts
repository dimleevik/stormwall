import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CreateUserMusics extends BaseSchema {
  protected tableName = "user_music";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id").primary();
      table.integer("user_id").unsigned().references("id").inTable("users");
      table.integer("music_id").unsigned().references("id").inTable("musics");
      table.timestamps(false);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
